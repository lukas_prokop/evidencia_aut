<?php
	include 'includes/class-autoloader.inc.php';
	include_once("classes/DB.class.php");


	$car = new car;
	$znacka = $car->getCarsByManufacturer("Suzuki");

 ?>

<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Vozidlá</title>
<meta značka="keywords" content="" />
<meta značka="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" media="all" />
<link href="fonts.css" rel="stylesheet" type="text/css" media="all" />


</head>
<body>
<div id="header-wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="index.php">Vozidlá</a></h1>
			<span></a></span> </div>
		<div id="menu">
			<ul>
        <li class="current_page_item"><a href="index.php" accesskey="1" title="">Domov</a></li>
				<li class="current_page_item"><a href="login.php" accesskey="1" title="">Odhlásiť sa</a></li>
				<?php
					if (isset($_SESSION['user']) and $_SESSION['user'] > 0){
						echo "<li><a href='logout.php'>Odhlásiť sa</a></li>";
					}
						?>

			</ul>
		</div>
	</div>
</div>

<div id="banner-wrapper">
	<div id="banner" class="container">
		<p>Toto je Evidencia firemných vozidiel a strojov značky Suzuki.</p>
	</div>
</div>
<div id="wrapper">
	<div id="featured-wrapper">
		<div id="featured" class="container">
      <table>
        <tr>
        <th>Značka</th>
        <th>Model</th>
        <th>ŠPZ</th>
        <th>VIN</th>
        <th>Rok Výroby</th>
      </tr>
<?php

 foreach ($znacka as $key => $znacka): ?>


                      <tr>
                           <td> <?php echo $znacka['Značka']; ?></td>
                           <td> <?php echo $znacka['Model']; ?>	</td>
                           <td> <?php echo $znacka['ŠPZ']; ?>		</td>
                           <td> <?php echo $znacka['VIN']; ?>		</td>
                           <td> <?php echo $znacka['Rok výroby']; ?></td>
                           <br>
                      </tr>


                    <?php endforeach; ?>
        </table>

		</div>
	</div>
</div>




</body>
</html>
