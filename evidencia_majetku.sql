-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hostiteľ: 127.0.0.1
-- Čas generovania: St 02.Dec 2020, 15:31
-- Verzia serveru: 10.4.8-MariaDB
-- Verzia PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `evidencia_majetku`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Sťahujem dáta pre tabuľku `customer`
--

INSERT INTO `customer` (`id`, `name`, `password`) VALUES
(2, 'lukas', '$2y$10$Bo5QOffJW0Tbd0I6cnxXXu0tTNbBA.xucF9/r1L8vJeigWpaLGupi'),
(5, 'lulu', '$2y$10$sc87ouu1SbThdbeXOGRcK.mk7g9nHkqkdrsQaJArPYWwcMwc5gaaW'),
(21, 'Ivka', '$2y$10$oU0dCTWOmFPFuderZE2nCe.CpFtphvPsyYwh3q1v8SSoQppZc6gPO');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `vozidla`
--

CREATE TABLE `vozidla` (
  `ID` int(11) NOT NULL,
  `Značka` varchar(255) NOT NULL,
  `Model` varchar(255) NOT NULL,
  `ŠPZ` varchar(255) NOT NULL,
  `VIN` varchar(255) NOT NULL,
  `Rok výroby` date NOT NULL,
  `STK/EK` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Sťahujem dáta pre tabuľku `vozidla`
--

INSERT INTO `vozidla` (`ID`, `Značka`, `Model`, `ŠPZ`, `VIN`, `Rok výroby`, `STK/EK`) VALUES
(1, 'Audi', 'A4 3.0TDI', 'NR456KT', 'WAUZZZ8KXFA097649', '2015-04-02', NULL),
(2, 'Audi', 'A4 2.0TDI', 'NR352HS', 'WAUZZZ8K3AA108533', '2010-07-14', NULL),
(3, 'Volvo', 'Nákladne vozidlo', 'NR569HN', 'YV2J4CLA73B329596', '2003-10-06', NULL),
(4, 'Manitou', 'MRT 2150 P', 'NRZ517', 'PR5110766839', '2015-11-16', NULL),
(5, 'Volkswagen', 'Amarok', 'NR727HS', 'WV1ZZZ2HZEH016431', '2014-12-23', NULL),
(6, 'Caterpillar', 'Stavebný stroj', 'NRZ645', 'CAT0432EKBXE00927', '2007-01-11', NULL),
(7, 'BMW', 'R 1150 GS', 'NR760AL', 'WB10415A3YZE00511', '1999-08-19', NULL),
(8, 'BMW', 'R 1200 GS', 'NR988JM', 'WB10307A35ZN34292', '2004-09-17', NULL),
(9, 'Suzuki', 'DL 650 A', 'NR301JK', 'JS1C7111100112204', '2014-01-13', NULL),
(10, 'Citroën', 'Jumper', 'NR207HB', 'VF7YBBMFC11694642', '2009-12-21', NULL),
(11, 'Citroën', 'Berlingo', 'NR810EN', 'VF7GJRHYK93068224', '2003-09-10', NULL);

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `vozidla`
--
ALTER TABLE `vozidla`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pre tabuľku `vozidla`
--
ALTER TABLE `vozidla`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
