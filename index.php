<?php
session_start();
?>
<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Vozidlá</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" media="all" />
<link href="fonts.css" rel="stylesheet" type="text/css" media="all" />


</head>
<body>
<div id="header-wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="index.php">Vozidlá</a></h1>
			<span></a></span> </div>
		<div id="menu">
			<ul>
				<?php
				if ($_SESSION['user'] == 2){
					echo '<li class="current_page_item"><a href="admin.php" accesskey="1" title="">ADMIN</a></li>';

				}
				if (isset($_SESSION['user']) and $_SESSION['user'] > 0){
					echo "<li><a href='logout.php'>Odhlásiť sa</a></li>";
				}
				?>
			</ul>
		</div>
	</div>
</div>

<div id="banner-wrapper">
	<div id="banner" class="container">
		<p>Toto je Evidencia firemných vozidiel a strojov.</p>
	</div>
</div>
<div id="wrapper">
	<div id="featured-wrapper">
		<div id="featured" class="container">
			<a href="audi.php">
			<div class="column1"> <img src="images/audi.png">
			</a>
				<div class="title">
					<h2>Vozidlá značky Audi</h2>
				</div>
						</div>
						<a href="vw.php">
			<div class="column2"> <img src="images/volkswagen.png">
				<div class="title">
					<h2>Vozidlá značky volkswagen</h2>
				</div>
			</div>
			<a href="citroen.php">
			<div class="column3"> <img src="images/citroen.png">
				<div class="title">
					<h2>Vozidlá značky Citroën</h2>
				</div>
			</div>
			<a href="BMW.php">
			<div class="column4"> <img src="images/bmw.png"><br>
				<div class="title">
					<h2>Vozidlá značky BMW</h2>
				</div>
			</div>
			<a href="volvo.php">
			<div class="column5"> <img src="images/Volvo.png"><br>
				<div class="title">
					<h2>Vozidlá značky Volvo</h2>
				</div>
			</div>
			<a href="suzuki.php">
			<div class="column6"> <img src="images/Suzuki.png">
				<div class="title">
					<h2>Vozidlá značky Suzuki</h2>
				</div>
			</div>
			<a href="manitou.php">
			<div class="column7"> <img src="images/MMM.png">
				<div class="title">
					<h2>Vozidlá značky Manitou</h2>
				</div>
			</div>
			<a href="cat.php">
			<div class="column8"> <img src="images/Caterpillar.png">
				<div class="title">
					<h2>Vozidlá značky Caterpillar</h2>
				</div>
			</div>
		</div>
	</div>




</body>
</html>
