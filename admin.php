<?php
session_start();
include 'includes/class-autoloader.inc.php';
$car = new Car;
$edit_state = false;

  //fetch the record to be updated
  if(isset($_GET['edit'])) {
     $id = $_GET['edit'];

     $edit_state = true;

     $record = $car->getCarById($id);
     $znacka = $record[0]['Značka'];
     $model = $record[0]['Model'];
     $id = $record[0]['ID'];
     $spz = $record[0]['ŠPZ'];
     $VIN = $record[0]['VIN'];
     $rok_vyroby = $record[0]['Rok výroby'];
  }

  if(isset($_POST['save']) || isset($_POST['update'])){
    $id = $_POST['id'];
    $znacka = $_POST['znacka'];
    $model = $_POST['model'];
    $spz = $_POST['spz'];
    $vin = $_POST['vin'];
    $rok_vyroby = $_POST['rok_vyroby'];
    if($edit_state == false){
      $car->addCar($znacka, $model, $spz, $vin, $rok_vyroby);
    } else {
      $car->updateCar($id, $znacka, $model, $spz, $vin, $rok_vyroby);
    }
  }

  if(isset($_GET['del'])){
    $car->deleteCar($_GET['del']);
  }
  ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Admin panel</title>
    <link rel="stylesheet" type="text/css" href="style.css">

  </head>
  <body>
    			<h1><a href="index.php">Vozidlá</a></h1>



    <?php
    //var_dump($_SESSION);
    if(isset($_SESSION['msg'])){ ?>
        <div class="msg">
          <?php
              echo $_SESSION['msg'];
              unset($_SESSION['msg']);
           ?>
         </div>
<?php }?>
    <table>
      <thead>
          <tr>
            <th>Značka</th>
            <th>Model</th>
            <th>ŠPZ</th>
            <th>VIN</th>
            <th>Rok Výroby</th>
            <th colspan="2">Action</th>
          </tr>
      </thead>
      <tbody>
          <?php
          $record = $car->getCars();
          foreach($record as $row) {?>

            <tr>
              <td><?php echo $row['Značka']; ?></td>
              <td><?php echo $row['Model']; ?></td>
              <td><?php echo $row['ŠPZ']; ?></td>
              <td><?php echo $row['VIN']; ?></td>
              <td><?php echo $row['Rok výroby']; ?></td>
              <td>
                <a class="edit_btn" href="admin.php?edit=<?php echo $row['ID']; ?>" >Edit</a>
              </td>
              <td>
                <a class="del_btn" href="admin.php?del=<?php echo $row['ID']; ?>" >Delete</a>
              </td>
            </tr>
          <?php } ?>
      </tbody>
    </table>
    <form method="POST" action="">
      <input type="hidden" name="id" value="<?php if(isset($id)) echo $id; ?>">

        <div class="input-group">
          <label>Značka</label>
          <input type="text" name="znacka" value="<?php if(isset($znacka)) echo $znacka; ?>">
        </div>
        <div class="input-group">
          <label>Model</label>
          <input type="text" name="model" value="<?php if(isset($model)) echo $model; ?>">
        </div>
        <div class="input-group">
          <label>ŠPZ</label>
          <input type="text" name="spz" value="<?php if(isset($spz)) echo $spz; ?>">
        </div>
        <div class="input-group">
          <label>VIN</label>
          <input type="text" name="vin" value="<?php if(isset($VIN)) echo $VIN; ?>">
        </div>
        <div class="input-group">
          <label>Rok výroby</label>
          <input type="date" name="rok_vyroby" value="<?php if(isset($rok_vyroby)) echo $rok_vyroby; ?>">
        </div>
        <div class="input-group">
          <?php if ($edit_state == false){
            echo '<button type="submit" name="save" class="btn">Save</button>';
          } else {
            echo '<button type="submit" name="update" class="btn">Update</button>';
          } ?>
        </div>
    </form>
  </body>
</html>
