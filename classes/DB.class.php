<?php

class DB { //vytvorenie triedy
  private $host = "localhost";
  private $port = 3306;
  private $user = "root";
  private $pwd = "";
  private $dbName = "evidencia_majetku";

  protected function connect() {
    try {
      $dsn = 'mysql:host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->dbName;
      $pdo = new PDO($dsn, $this->user, $this->pwd);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
      return $pdo;
    } catch(PDOException $e) {
      echo "Connection failed: " . $e->getMessage();
    }
  }
}
?>
