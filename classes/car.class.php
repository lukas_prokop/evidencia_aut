<?php
class Car extends DB {

  public function getCarsByManufacturer($znacka) {
    $pdo = $this->connect();
    $sql = "SELECT * FROM vozidla WHERE Značka = ?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$znacka]);
    return $stmt->fetchAll();

  }

  public function getCarById($id) {
    $pdo = $this->connect();
    $sql = "SELECT * FROM vozidla WHERE id = ?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$id]);
    return $stmt->fetchAll();

  }

  // nahrávanie do db
  public function addCar($znacka, $model, $spz, $vin, $rok_vyroby){
    $pdo = $this->connect();
    $sql = "INSERT INTO `vozidla` (`Značka`, `Model`, `ŠPZ`, `VIN`, `Rok výroby`) VALUES (?,?,?,?,?)";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$znacka, $model, $spz, $vin, $rok_vyroby]);
    $_SESSION['msg'] = "Záznam uložený";
    header('location: admin.php');  
}

  // edit db
  public function updateCar($id, $znacka, $model, $spz, $vin, $rok_vyroby){
    $pdo = $this->connect();
    $sql = "UPDATE vozidla SET `Značka`=?, `Model`=?, `ŠPZ`=?, `VIN`=?, `Rok výroby`=? WHERE ID=?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$znacka, $model, $spz, $vin, $rok_vyroby, $id]);
    $_SESSION['msg'] = "Záznam upravený";
    header('location: admin.php');
}

  // vymazávanie z db
  public function deleteCar($id){
    $pdo = $this->connect();
    $sql = "DELETE FROM vozidla WHERE ID=?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$id]);
    $_SESSION['msg'] = "Záznam vymazaný";
    header('location: admin.php');
  }

  // načítanie  z db
  public function getCars(){
    $pdo = $this->connect();
    $sql = "SELECT * FROM vozidla WHERE ?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([1]);
    return $stmt->fetchAll();
  }
}
