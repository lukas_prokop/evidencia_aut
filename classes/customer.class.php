<?php

class Customer extends DB {

  protected function getCustomerData($id) {
    $sql = "SELECT * FROM customer WHERE id = ?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$id]);
    $results = $stmt->fetchAll();
    return $results;
  }

  protected function getCustomerId($name) {
    $sql = "SELECT id FROM customer WHERE name = ?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$name]);
    $results = $stmt->fetchAll();
    //var_dump($results);
    return intval($results[0]["id"]);
  }

  protected function getCustomerPassword($name) {
    $sql = "SELECT password FROM customer WHERE name = ?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$name]);
    $results = $stmt->fetchAll();
    return $results[0]['password'];
  }

  protected function setCustomer($name, $password) {
    $sql = "INSERT INTO customer (`name`, `password`) VALUES (?, ?)";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$name, $password]);
  }


}
