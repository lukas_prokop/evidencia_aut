<?php

spl_autoload_register('autoLoader');

function autoLoader($className){
  $path = "classes/";
  $extension = ".class.php";
  $fileName = $path . $className . $extension;
  $fileName = strtolower($fileName);

  if (!file_exists($fileName)) return false;
  include_once $fileName;
}
